# Docker python и nginx
Разработан сервис на Flask и реализацией GET-запроса, который выводить "Hello World", маршурутизацией с помощью nginx и запущенными в docker. А также запустить их совместно с помощью Docker-compose.

Файлы для python находятся здесь:
[python-docker](/python-docker)

- [app.py](/python-docker/app.py) - приложение на фраемворке Flask
- [requirements.txt](/python-docker/requirements.txt) -
 зависимости для виртуального окружения
- [Dockerfile](/python-docker/Dockerfile) - файл для сборки Docker контейнера python

Файлы для nginx находятся здесь:
[nginx](/nginx)

- [default.conf](nginx/default.conf) - конфигурация nginx
- [Dockerfile](/nginx/Dockerfile) - файл для сборки Docker контейнера nginx

Файл для docker-compose - [docker-compose.yml](docker-compose.yml)
# Требования:
- ubuntu-22.04
- python:3.8 или выше
- [docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
- [docker-compose](https://docs.docker.com/compose/install/linux/#install-using-the-repository)

# Настроим переменные среды c ip и портом нашего сервиса следующими командами:
```
export REST_IP=0.0.0.0
export REST_PORT=5000
```
Проверим, что заданы успешно:
```
printenv REST_IP
printenv REST_PORT
```
Пример:
```
root@Linux:/home/adminuser# export REST_IP=0.0.0.0
root@Linux:/home/adminuser# export REST_PORT=5000
root@Linux:/home/adminuser# printenv REST_IP
0.0.0.0
root@Linux:/home/adminuser# printenv REST_PORT
5000
```
# Запускаем проект.
Устанавливаем python:
```
sudo apt-get update && sudo apt-get install python3
```
Для запуска создадим отдельное виртуальное окружение со своим набором библиотек. Находясь в корне. 
Создание окружения:
```
python3 -m venv python-docker
```
Переходим в директорию
```
cd /python-docker
```
Активируем окружение
```
source ./bin/activate
```
Должнен появиться префикс "(python-docker) root@Linux:/Устанавливаем зависимости
```
pip3 install -r requirements.txt
```
Запускаем проект
```
python3 -m flask run --host=$REST_IP --port=$REST_PORT
```
Проверяем работу сервиса с помощью Curl:
```
curl http://0.0.0.0:5000
```
После работы деактивируем окружение
```
deactivate
```

# Создаем Docker image для python. 
Файл настроек [Dockerfile](/python-docker/Dockerfile)
```
# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

CMD python3 -m flask run --host=$REST_IP --port=$REST_PORT
```
Собираем, запускаем, тестируем curl.
```
cd python-docker
docker build --rm -t python-docker .
docker run --name python-docker --publish 5000:5000 -e REST_IP=$REST_IP -e REST_PORT=$REST_PORT python-docker
curl http://0.0.0.0:5000
```
Результат:
```
adminuser@Linux:~$ curl http://0.0.0.0:5000
Hello World
```

# Создаем Docker image для nginx.
Файл настроек [Dockerfile](/nginx/Dockerfile)
```FROM nginx
RUN rm /etc/nginx/conf.d/default.conf
COPY default.conf /etc/nginx/conf.d/default.conf```

Файл конфига. Изменить ip сервера на свой.

```upstream rest {
	server 10.0.2.15:5000;
}

server {
    listen 8080;
    listen [::]:8080;
        
    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_buffering off;
        proxy_pass http://rest;
    }
}
```
Собираем, запускаем, тестируем curl.
```
cd ..
cd nginx
docker build --rm -t nginx .
docker run --name nginx --publish 8080:8080 nginx
curl http://0.0.0.0:8080
```
Результат:
```
adminuser@Linux:~$ curl http://0.0.0.0:8080
Hello World
```

# Запускаем docker-compose 
Файл настроек [docker-compose](https://docs.docker.com/compose/install/linux/#install-using-the-repository)
```version : "3"
services:
  python-rest:
    build: python-docker/
    ports:
      - "5000:5000"
    environment:
      - REST_IP=0.0.0.0
      - REST_PORT=5000
    depends_on:
      - nginx
  nginx:
    build: nginx/
    ports:
      - "8080:8080"
```
- build - образы docker
- ports - проброс портов(внешний:внутренний)
- environment - задаем переменные окружения
- depends_on - задаем зависимость запуска, сервис на Flask будет ждать запуска nginx

Проверим docker-compose
```
cd ..
docker compose up
curl http://0.0.0.0:8080
```
Результат:
```
adminuser@Linux:~$ curl http://0.0.0.0:8080
Hello World
```
